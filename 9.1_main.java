package com.company;
/**
 Сделать класс Figure, у данного класса есть два поля - x и y координаты.
 Классы Ellipse и Rectangle должны быть потомками класса Figure.
 Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
 В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.
 */
public class Main {

    public static void main(String[] args) {
        Figure [] figures = new Figure[4]; // констуктор с использованием 4 фигур

        figures[0] = new Rectangle(0,0, 3, 4);
        figures[2] = new Square(0,0,5 );
        figures[1] = new Ellipse(0, 0, 2, 3);
        figures[3] = new Circle(0, 0, 5);

        System.out.println(figures[0].getPerimeter());
        System.out.println(figures[1].getPerimeter());
        System.out.println(figures[2].getPerimeter());
        System.out.println(figures[3].getPerimeter());
        
    }
}