package com.company;

public class Square extends Rectangle {
    // класс квадрат
    public Square(int x, int y, double a) { // все стороны ровны, поэтому можно указать одну сторону
        super(x, y, a, a); // выхов конструктора предка
    }
    public double getPerimeter() {
        return 4 * a;
    }
}